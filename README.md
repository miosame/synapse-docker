# RTFM

First step you need to do is read the fucking manual, step: `Generating a configuration file` here: https://hub.docker.com/r/matrixdotorg/synapse

`SYNAPSE_SERVER_NAME` will be your subdomain, in the examples it's: `subdomain.whateverthefuck.com`

# Replace values

Replace `subdomain.whateverthefuck.com` and `whateverthefuck.com` respectively in:

- `nginx-proxy/site.conf`
- `nginx-proxy/html/.well-known/matrix`

Replace default generated password `POSTGRES_PASSWORD` in: `docker-compose.yml`

# Optional

If you want to serve some additional shit, drop it into the `html` folder, it'll get served first before falling back to synapse/matrix reverse proxy.